import sbt._

object Dependencies {
  lazy val sparkSql = "org.apache.spark" %% "spark-sql" % "2.3.0"
  lazy val scalaJacks = "com.lambdaworks" %% "jacks" % "2.3.3"
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.8" % "test"
}
