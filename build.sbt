import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      scalaVersion := "2.11.8",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "grid-dynamics-test",
    libraryDependencies ++= Seq(sparkSql, scalaJacks, scalaTest),
    mainClass in (Compile, run) := Some("griddynamics.test.spark.ClickStreamAnalytics")
  )
