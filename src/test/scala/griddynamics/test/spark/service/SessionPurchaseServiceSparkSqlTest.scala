package griddynamics.test.spark.service
import org.apache.spark.sql.{ SparkSession}

class SessionPurchaseServiceSparkSqlTest extends SessionPurchaseServiceTest {

  override def createSessionPurchaseService(sparkSession: SparkSession): SessionPurchaseService =
    new SessionPurchaseServiceSparkSql(sparkSession)

}
