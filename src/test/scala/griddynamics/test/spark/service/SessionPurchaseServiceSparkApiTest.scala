package griddynamics.test.spark.service

import org.apache.spark.sql.SparkSession

class SessionPurchaseServiceSparkApiTest extends SessionPurchaseServiceTest {

  override def createSessionPurchaseService(sparkSession: SparkSession): SessionPurchaseService =
    new SessionPurchaseServiceSparkApi(sparkSession)

}
