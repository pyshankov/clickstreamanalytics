package griddynamics.test.spark.service

import griddynamics.test.spark.model.{MobileClickEvent, Purchase, SessionPurchaseEvent}
import org.apache.spark.sql.internal.SQLConf.SHUFFLE_PARTITIONS
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}
import org.scalatest.{BeforeAndAfterEach, FunSuite}

//sometimes good, sometimes not...
//but some common test cases can be extracted here...
trait SessionPurchaseServiceTest extends FunSuite with BeforeAndAfterEach {

  private val master = "local[*]"

   var sparkSession: SparkSession = null
  private var sessionPurchaseService: SessionPurchaseService = null

  //create spark context for each test execution,  same context is reused
  // when test are running in parallel...
  override protected def beforeEach(): Unit = {
    sparkSession = new SparkSession.Builder()
      .appName("SessionPurchaseServiceSparkSqlTest")
      .master(master)
      .getOrCreate()
    val defaultParallelism: Int = sparkSession.sparkContext.defaultParallelism
    sparkSession.sessionState.conf.setConf(SHUFFLE_PARTITIONS, defaultParallelism)

    sessionPurchaseService = createSessionPurchaseService(sparkSession)
  }

  override protected def afterEach(): Unit = {
    sparkSession.stop()
  }


  test("sessionPurchaseService.defineSessionPurchaseEvents positive case count") {
    val input: Dataset[MobileClickEvent] =
      sourceMobileEvents(sparkSession, "src/test/resources/data/input/mobile-app-clickstream_sample.csv")

    val expected: Dataset[SessionPurchaseEvent] =
      sourceSessionPurchaseEvents(sparkSession,"src/test/resources/data/input/SessionPurchaseEvent.csv")

    val actual: Dataset[SessionPurchaseEvent] = sessionPurchaseService.defineSessionPurchaseEvents(input)

    assert(expected.count() == actual.count())
  }


  def sourceSessionPurchaseEvents(spark: SparkSession, filePath: String): Dataset[SessionPurchaseEvent] = spark.read
    .format("com.databricks.spark.csv")
    .option("delimiter", ",")
    .option("escape", "\"")
    .option("quote", "\"")
    .option("header", true)
    .load(filePath)
    .map(SessionPurchaseEvent.mapRowToObj)(Encoders.product[SessionPurchaseEvent])


  def sourcePurchases(spark: SparkSession, filePath: String): Dataset[Purchase] = spark.read
    .format("com.databricks.spark.csv")
    .option("delimiter", ",")
    .option("escape", "\"")
    .option("quote", "\"")
    .option("header", true)
    .load(filePath)
    .map(Purchase.mapRowToObj)(Encoders.product[Purchase])

  def sourceMobileEvents(spark: SparkSession, filePath: String): Dataset[MobileClickEvent] = spark.read
    .format("com.databricks.spark.csv")
    .option("delimiter", ",")
    .option("escape", "\"")
    .option("quote", "\"")
    .option("header", true)
    .load(filePath)
    .map(MobileClickEvent.mapRowToObj)(Encoders.product[MobileClickEvent])

  def createSessionPurchaseService(sparkSession: SparkSession): SessionPurchaseService

}
