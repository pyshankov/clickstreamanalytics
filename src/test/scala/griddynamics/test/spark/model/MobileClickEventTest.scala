package griddynamics.test.spark.model

import org.scalatest.FunSuite

//Same tests with each method model
class MobileClickEventTest extends FunSuite {

  test("MobileClickEvent.parseAttributes positive case") {

    val input: String = "{{\"campaign_id\": \"cmp1\",  \"channel_id\": \"Google Ads\"}}"
    val expetedResult: Option[Map[String,String]] = Some(Map("campaign_id" -> "cmp1",  "channel_id" -> "Google Ads"))

   val actualResult: Option[Map[String,String]] =  MobileClickEvent.parseAttributes(input)

    assert(expetedResult == actualResult)
  }


  test("MobileClickEvent.parseAttributes not valid json") {
    val input: String = ""

    val thrown = intercept[Exception] {
      MobileClickEvent.parseAttributes(input)
    }

    assert(thrown != null)
  }

}
