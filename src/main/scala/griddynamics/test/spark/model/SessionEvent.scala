package griddynamics.test.spark.model

import org.apache.spark.sql.Row

case class SessionEvent(
                         purchaseIds: Set[String], //Within the same session, there can be multiple purchases
                         sessionId: String,
                         campaignId: String,
                         channelId: String
                       )

object SessionEvent {


  def mapFromClickEventRow(row: Row): SessionEvent =
    SessionEvent(
      Set(Option.apply(row.getAs[Map[String,String]]("attributes")).getOrElse(Map()).getOrElse("purchase_id", null)),
      row.getAs[String]("sessionId"),
      Option.apply(row.getAs[Map[String,String]]("attributes")).getOrElse(Map()).getOrElse("campaign_id", null),
      Option.apply(row.getAs[Map[String,String]]("attributes")).getOrElse(Map()).getOrElse("channel_id", null)
    )

  /*
  *  mergePurchaseEvent is commutative and transitive.
  *  as far as merge is done on the data that don't intersect on event level,
  *  it wouldn't overlay. (except 'purchase' event, that can appear within the same session).
  *  purchase_id can be accumulated into array, and then additional flatMap can be applied to unwind elements
  *  */
  def mergePurchaseEvent(p1: SessionEvent, p2: SessionEvent): SessionEvent =
    SessionEvent(
      p1.purchaseIds ++ p2.purchaseIds,
      if(p1.sessionId != null) p1.sessionId else p2.sessionId,
      if(p1.campaignId != null) p1.campaignId else p2.campaignId,
      if(p1.channelId != null) p1.channelId else p2.channelId
    )

  def convertToSessionPurchaseEvent(sessionEvent: SessionEvent): Set[SessionPurchaseEvent] =
    sessionEvent.purchaseIds.map(purchaseId=>
      SessionPurchaseEvent(
        purchaseId,
        sessionEvent.sessionId,
        sessionEvent.campaignId,
        sessionEvent.channelId)
    )

}
