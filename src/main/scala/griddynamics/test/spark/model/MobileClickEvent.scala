package griddynamics.test.spark.model

import java.sql.Timestamp

import com.lambdaworks.jacks.JacksMapper
import org.apache.spark.sql.Row


case class MobileClickEvent(
                             userId: String,
                             eventId: String,
                             eventTime: Timestamp,
                             eventType: String, //could be replaced with Enum, don't have a time:)
                             attributes: Option[Map[String,String]])

object MobileClickEvent {

  val userId: String = "userId"
  val eventId: String = "eventId"
  val eventTime: String = "eventTime"
  val eventType: String = "eventType"
  val attributes: String = "attributes"

  val APP_OPEN_EVENT: String = "app_open"
  val SEARCH_PRODUCT_EVENT: String = "search_product"
  val VIEW_PRODUCT_EVENT: String = "view_product_details"
  val PURCHASE_EVENT: String = "purchase"
  val APP_CLOSE_EVENT: String = "app_close"

    def mapRowToObj(row: Row): MobileClickEvent =
      MobileClickEvent(
        row.getAs[String]("userId"),
        row.getAs[String]("eventId"),
        Timestamp.valueOf(row.getAs[String]("eventTime")),
        row.getAs[String]("eventType"),
        parseAttributes(row.getAs[String]("attributes"))
      )

    def parseAttributes(json: String): Option[Map[String,String]] =
      if (json != null)
        Option.apply(
          JacksMapper.readValue[Map[String,String]](   //ignore or not parsing exception? let's it throw:)
            json
              .replace("{{", "{")
              .replace("}}", "}")
          )
        )
      else Option.empty
}


