package griddynamics.test.spark.model

import java.sql.Timestamp

import org.apache.spark.sql.Row

case class PurchaseAttribution(
                                purchaseId:  String,
                                purchaseTime: Timestamp,
                                billingCost: Double,
                                isConfirmed: Boolean,
                                sessionId: String,
                                campaignId: String,
                                channelId: String
                              )

 object PurchaseAttribution {

  val purchaseId: String = "purchaseId"
  val purchaseTime: String = "purchaseTime"
  val billingCost: String = "billingCost"
  val isConfirmed: String = "isConfirmed"
  val sessionId: String = "sessionId"
  val campaignId: String = "campaignId"
  val channelId: String = "channelId"

  def mapFromRow(row: Row): PurchaseAttribution =
    PurchaseAttribution(
      row.getAs[String](purchaseId),
      row.getAs[Timestamp](purchaseTime),
      row.getAs[Double](billingCost),
      row.getAs[Boolean](isConfirmed),
      row.getAs[String](sessionId),
      row.getAs[String](campaignId),
      row.getAs[String](channelId)
    )
}

