package griddynamics.test.spark


import org.apache.spark.sql.Row


package object model {


  case class SessionPurchaseEvent(
    purchaseId: String,
    sessionId: String,
    campaignId: String,
    channelId: String
  )

  case class CampaignResult(
    campaignId: String,
    billingCost: Double
  )

  case class ChannelResult(
    channelId: String,
    engagements: Double
  )

  object SessionPurchaseEvent {
    val purchaseId: String = "purchaseId"
    val sessionId: String = "sessionId"
    val campaignId: String = "campaignId"
    val channelId: String = "channelId"

    def mapRowToObj(row: Row): SessionPurchaseEvent =
      SessionPurchaseEvent(
        row.getAs[String]("purchaseId"),
        row.getAs[String]("sessionId"),
        row.getAs[String]("campaignId"),
        row.getAs[String]("channelId")
      )
  }


  object CampaignResult {
    val totalBillingCost: String = "totalBillingCost"
    val campaignId: String = "campaignId"

    def mapRowToObj(row: Row): CampaignResult =
      CampaignResult(
        row.getAs[String]("campaignId"),
        row.getAs[Double]("totalBillingCost")
      )
  }

  object ChannelResult {
    val channelId: String = "channelId"
    val engagements: String = "engagements"

    def mapRowToObj(row: Row): ChannelResult =
      ChannelResult(
        row.getAs[String]("channelId"),
        row.getAs[Long]("engagements")
      )
  }


}
