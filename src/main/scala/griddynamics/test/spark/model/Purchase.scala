package griddynamics.test.spark.model

import java.sql.Timestamp

import org.apache.spark.sql.Row

case class Purchase(
                     purchaseId:  String,
                     purchaseTime: Timestamp,
                     billingCost: Double,
                     isConfirmed: Boolean
                   )

object Purchase {

  val purchaseId: String = "purchaseId"
  val purchaseTime: String = "purchaseTime"
  val billingCost: String = "billingCost"
  val isConfirmed: String = "isConfirmed"

  def mapRowToObj(row: Row): Purchase =
    Purchase(
      row.getAs[String](purchaseId),
      Timestamp.valueOf(row.getAs[String](purchaseTime)),
      row.getAs[String](billingCost).toDouble,
      row.getAs[String](isConfirmed).toBoolean
    )
}
