package griddynamics.test.spark.service

import griddynamics.test.spark.model
import griddynamics.test.spark.model.{CampaignResult, ChannelResult, MobileClickEvent, PurchaseAttribution, SessionEvent, SessionPurchaseEvent}
import griddynamics.test.spark.udaf.SessionUDAF
import org.apache.spark.sql.expressions.{Window}
import org.apache.spark.sql.expressions.scalalang.typed
import org.apache.spark.sql.functions.{col, countDistinct, desc}
import org.apache.spark.sql.{Column, Dataset, Encoders, SparkSession}

class SessionPurchaseServiceSparkApi(sparkSession: SparkSession) extends SessionPurchaseService  {

  override def defineSessionPurchaseEvents(mobileEvents: Dataset[model.MobileClickEvent]): Dataset[model.SessionPurchaseEvent] = {
    val sessionColumnDef: Column =
      SessionPurchaseServiceSparkApi
      .sessionUDAF(col(MobileClickEvent.eventType), col(MobileClickEvent.eventId))
      .over(Window.partitionBy(MobileClickEvent.userId)
        .rowsBetween(Window.unboundedPreceding, Window.currentRow))

    mobileEvents
      .withColumn(SessionPurchaseEvent.sessionId, sessionColumnDef)
      .map(SessionEvent.mapFromClickEventRow)(Encoders.product[SessionEvent])
      .groupByKey(_.sessionId)(Encoders.STRING)
      .reduceGroups((p1,p2) => SessionEvent.mergePurchaseEvent(p1,p2))
      .flatMap(sessionEvent => SessionEvent.convertToSessionPurchaseEvent(sessionEvent._2))(Encoders.product[SessionPurchaseEvent])
  }

  override def calculateTopNChannels(purchaseAttributions: Dataset[model.PurchaseAttribution], n: Int): Array[model.ChannelResult] =
    purchaseAttributions
      .groupBy(PurchaseAttribution.channelId)
      .agg(countDistinct(PurchaseAttribution.sessionId))
      .toDF(ChannelResult.channelId, ChannelResult.engagements)
      .sort(desc(ChannelResult.engagements))
      .map(ChannelResult.mapRowToObj)(Encoders.product[ChannelResult])
      .head(n)

  override def calculateTopNCampaigns(purchaseAttributions: Dataset[model.PurchaseAttribution], n: Int): Array[model.CampaignResult] =
  purchaseAttributions
    .groupByKey(_.campaignId)(Encoders.STRING)
    .agg(typed.sum[PurchaseAttribution](_.billingCost))
    .toDF(CampaignResult.campaignId, CampaignResult.totalBillingCost)
    .sort(desc(CampaignResult.totalBillingCost))
    .map(CampaignResult.mapRowToObj)(Encoders.product[CampaignResult])
    .head(n)


  object SessionPurchaseServiceSparkApi {
    val sessionUDAF: SessionUDAF = new SessionUDAF()
  }

}
