package griddynamics.test.spark.service
import griddynamics.test.spark.model
import griddynamics.test.spark.model.{CampaignResult, ChannelResult, MobileClickEvent, Purchase, PurchaseAttribution, SessionEvent, SessionPurchaseEvent}
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

class SessionPurchaseServiceSparkSql(sparkSession: SparkSession) extends SessionPurchaseService {

  override def defineSessionPurchaseEvents(mobileEvents: Dataset[model.MobileClickEvent]): Dataset[model.SessionPurchaseEvent] = {
    val table = "mobile_events"
    mobileEvents.createOrReplaceTempView(table)

    val sql =
      "SELECT *, CONCAT(" + MobileClickEvent.userId + ",sessionIdentifier) as " + SessionPurchaseEvent.sessionId +
      " FROM (" +
          "SELECT *, SUM(IF(isNewSession, 1, 0)) OVER w AS sessionIdentifier" +
        " FROM (" +
            "SELECT *, CAST(CASE WHEN eventType='" + MobileClickEvent.APP_OPEN_EVENT + "' and LAG(eventType) OVER w='" + MobileClickEvent.APP_CLOSE_EVENT + "' or LAG(eventType) OVER w is null then 1 ELSE 0 END as BOOLEAN) as isNewSession" +
          " FROM " + table + ") a" +
        " WINDOW w AS (PARTITION BY "+ MobileClickEvent.userId + " ORDER BY "+ MobileClickEvent.eventTime +"))"

    sparkSession.sql(sql)
      .map(SessionEvent.mapFromClickEventRow)(Encoders.product[SessionEvent])
      .groupByKey(_.sessionId)(Encoders.STRING)
      .reduceGroups((p1, p2) => SessionEvent.mergePurchaseEvent(p1, p2))
      .flatMap(sessionEvent => SessionEvent.convertToSessionPurchaseEvent(sessionEvent._2))(Encoders.product[SessionPurchaseEvent])
  }


  override def calculateTopNChannels(purchaseAttributions: Dataset[PurchaseAttribution], n: Int): Array[model.ChannelResult] = {
    val tableName = "purchase_attributions"
    purchaseAttributions.createOrReplaceTempView(tableName)

    val sql = "SELECT "+
      ChannelResult.channelId + "," +
    "COUNT(DISTINCT " + PurchaseAttribution.sessionId + ") AS " + ChannelResult.engagements +
      " from " + tableName +
      " GROUP BY " + ChannelResult.channelId +
      " ORDER BY " + ChannelResult.engagements + " DESC"

    sparkSession.sql(sql)
      .map(ChannelResult.mapRowToObj)(Encoders.product[ChannelResult])
      .head(n)
  }

  override def calculateTopNCampaigns(purchaseAttributions: Dataset[PurchaseAttribution], n: Int): Array[model.CampaignResult] = {
    val tableName = "purchase_attributions"
    purchaseAttributions.createOrReplaceTempView(tableName)

    val sql = "SELECT "+
      CampaignResult.campaignId + ", " +
      "sum(" + Purchase.billingCost + ") as " + CampaignResult.totalBillingCost +
      " from " + tableName +
      " GROUP BY " + CampaignResult.campaignId +
      " ORDER BY " + CampaignResult.totalBillingCost + " DESC"

    sparkSession.sql(sql)
      .map(CampaignResult.mapRowToObj)(Encoders.product[CampaignResult])
      .head(n)
  }

}
