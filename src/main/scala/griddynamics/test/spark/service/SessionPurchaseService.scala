package griddynamics.test.spark.service

import griddynamics.test.spark.model.{CampaignResult, ChannelResult, MobileClickEvent, PurchaseAttribution, SessionPurchaseEvent}
import org.apache.spark.sql.Dataset

trait SessionPurchaseService {

  def defineSessionPurchaseEvents(mobileEvents: Dataset[MobileClickEvent]): Dataset[SessionPurchaseEvent]

  def calculateTopNChannels(purchaseAttributions: Dataset[PurchaseAttribution], n: Int) : Array[ChannelResult]

  def calculateTopNCampaigns(purchaseAttributions: Dataset[PurchaseAttribution], n: Int) : Array[CampaignResult]
}
