package griddynamics.test.spark


import griddynamics.test.spark.model.{MobileClickEvent, Purchase, PurchaseAttribution, SessionPurchaseEvent}
import griddynamics.test.spark.service.{SessionPurchaseService, SessionPurchaseServiceSparkApi, SessionPurchaseServiceSparkSql}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.internal.SQLConf.SHUFFLE_PARTITIONS
import org.apache.spark.sql.{Dataset, Encoders, SparkSession}

object ClickStreamAnalytics {

  def main(args: Array[String]) {
    val spark = SparkSession.builder
//      .master("local[*]")
      .appName("Pavel Marchenko Grid Dynamics test")
      .getOrCreate()
    /*
      During execution of groupByKey,by default, spark context creates 200 partitions.
      Override this behaviour by creating 'defaultParallelism' partitions to process data
      more efficiently.
     */
    val defaultParallelism: Int = spark.sparkContext.defaultParallelism
    spark.sessionState.conf.setConf(SHUFFLE_PARTITIONS, defaultParallelism)

    val purchases: Dataset[Purchase] =
      sourcePurchases(spark, "src/main/resources/purchases_sample - purchases_sample.csv")
      .repartition(defaultParallelism,col("purchaseId"))

    val mobileEvents: Dataset[MobileClickEvent] =
     sourceMobileEvents(spark, "src/main/resources/mobile-app-clickstream_sample - mobile-app-clickstream_sample.csv")
      .repartition(defaultParallelism,col("userId"))


    executeExamples(new SessionPurchaseServiceSparkSql(spark),purchases,mobileEvents,defaultParallelism)
    executeExamples(new SessionPurchaseServiceSparkApi(spark),purchases,mobileEvents,defaultParallelism)

    spark.stop()
  }

  /*
    define 'purchases' Dataset
   */
  def sourcePurchases(spark: SparkSession, filePath: String): Dataset[Purchase] = spark.read
    .format("com.databricks.spark.csv")
    .option("delimiter", ",")
    .option("escape", "\"")
    .option("quote", "\"")
    .option("header", true)
    .load(filePath)
    .map(Purchase.mapRowToObj)(Encoders.product[Purchase])

  /*
    define 'mobile clickstream' Dataset
   */
  def sourceMobileEvents(spark: SparkSession, filePath: String): Dataset[MobileClickEvent] = spark.read
    .format("com.databricks.spark.csv")
    .option("delimiter", ",")
    .option("escape", "\"")
    .option("quote", "\"")
    .option("header", true)
    .load(filePath)
    .map(MobileClickEvent.mapRowToObj)(Encoders.product[MobileClickEvent])

  def executeExamples(sessionTransformation: SessionPurchaseService,
                      purchases: Dataset[Purchase],
                      mobileEvents: Dataset[MobileClickEvent],
                      defaultParallelism: Int): Unit = {

    val mobileSessions: Dataset[SessionPurchaseEvent] =
      sessionTransformation.defineSessionPurchaseEvents(mobileEvents)
    .repartition(defaultParallelism,col("purchaseId"))


    val purchaseAttributions: Dataset[PurchaseAttribution] =
    mobileSessions.join(purchases, Seq("purchaseId"), "left")
    .map(PurchaseAttribution.mapFromRow)(Encoders.product[PurchaseAttribution])

//    //    purchaseAttributions.explain()
    sessionTransformation.calculateTopNCampaigns(purchaseAttributions,10).foreach(println(_))
    sessionTransformation.calculateTopNChannels(purchaseAttributions,3).foreach(println(_))
  }




}
