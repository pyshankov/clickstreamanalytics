package griddynamics.test.spark.udaf

import griddynamics.test.spark.model.MobileClickEvent
import org.apache.spark.sql.Row
import org.apache.spark.sql.expressions.{MutableAggregationBuffer, UserDefinedAggregateFunction}
import org.apache.spark.sql.types.{DataType, StringType, StructField, StructType}

class SessionUDAF extends UserDefinedAggregateFunction {

  // Defind the schema of the input data
  override def inputSchema: org.apache.spark.sql.types.StructType =
    new StructType()
      .add(StructField("eventType", StringType))
      .add(StructField("eventId", StringType))

  // Define how the aggregates types will be
  override def bufferSchema: StructType = new StructType()
    .add(StructField("curEvent", StringType))
    .add(StructField("prevEvent", StringType))
    .add(StructField("sessionId", StringType))


  // define the return type
  override def dataType: DataType = StringType

  // Does the function return the same value for the same input?
  override def deterministic: Boolean = true

  // Initial values
  override def initialize(buffer: MutableAggregationBuffer): Unit = {
    buffer(0) = null
    buffer(1) = null
    buffer(2) = null
  }

  // Updated based on Input
  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {
    buffer(1) = buffer(0)
    buffer(0) = input.getAs[String](0)
    if(buffer(0) == MobileClickEvent.APP_OPEN_EVENT && (buffer(1) == null || buffer(1) == MobileClickEvent.APP_CLOSE_EVENT))
      buffer(2) = input.getAs[String](1)
  }

  // Merge two schemas
  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {
  }

  // Output
  override def evaluate(buffer: Row): Any = {
    buffer.getAs[String](2)
  }


}